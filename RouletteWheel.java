package roulette;
import java.util.Random;

public class RouletteWheel {
    private Random rand = new Random();
    private final int WHEEL_SIZE = 35; // 0-35 (36)
    private int number;
    private boolean low;
    private boolean even;
    
    public RouletteWheel(){
        this.number = 0;
        this.low = false;
        this.even = false;
    }

    public void spin(){
        this.number = rand.nextInt(WHEEL_SIZE);
        if (this.number <= WHEEL_SIZE/2) {
            this.low = true;
        }
        if (this.number%2 != 1) {
            this.even = true;
        }
    }

    public int getValue() {
        return this.number;
    }
    public boolean getLow() {
        return this.low;
    }
    public boolean getEven() {
        return this.even;
    }
}
