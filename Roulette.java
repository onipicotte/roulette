package roulette;
import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        Scanner key = new Scanner(System.in);
        int money = 100;
        int betNumber, betAmount;
        RouletteWheel wheelOne = new RouletteWheel();

        System.out.println("Welcome to the best Roulette game!");

        System.out.println("\nPlease enter your bet:\n(1-100)");
        betAmount = key.nextInt();
        money -= betAmount;

        System.out.println("Please enter the number you want to bet on:\n(0-36) ");
        betNumber = key.nextInt();

        System.out.println("\nRolling the Roulette...\n");
        wheelOne.spin();

        if (wheelOne.getValue() == betNumber) {
            System.out.println("You won!\nYou have bet on " + betNumber + " and the wheel stopped at the number " + wheelOne.getValue());
            System.out.println("You won " + betAmount*35);
        } else if (wheelOne.getLow()) {
            
        } else if (wheelOne.getEven()) {
            
        } else {
            System.out.println("You lost.\nThe number rolled ");
        }

    }
    
}